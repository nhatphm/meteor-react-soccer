import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Players from '../api/players.js'
import {BrowserRouter} from 'react-router-dom';
import browserHistory from '../api/history';

export default class Example extends Component {
  submitPlayer(event) {
    event.preventDefault();
    let data = {
      name: this.refs.name.value,
      team: this.refs.team.value,
      ballManipulation: this.refs.ballManipulation.value,
      passingAbilities: this.refs.passingAbilities.value,
      notes: this.refs.notes.value,
      createAt: new Date(),
    }
   // console.log(data);
    Meteor.call('players.insert', data, (error, result) => {
      console.log(result);
          browserHistory.push('/');
    })

    // Players.insert({
    //   
    // });
  }
  render() {
    return (
      <div className="row">
        <form className="col s12" onSubmit={this.submitPlayer.bind(this)}>
          <h3>Add a new Player</h3>
          <div className="row">
            <div className="input-field col s6">
              <input type="text" placeholder="name" ref="name" className="validate" />
            </div>
            <div className="input-field col s6">
              <input type="text" placeholder="team" ref="team" className="validate" />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <input type="number" placeholder="ballManipulation" ref="ballManipulation" className="validate" />
            </div>
            <div className="input-field col s6">
              <input type="number" placeholder="passingAbilities" ref="passingAbilities" className="validate" />
            </div>
          </div>

          <div className="row">
            <div className="input-field col s6">
              <textarea type="text" placeholder="notes" ref="notes" className="materialize-textarea" />
            </div>
            <div className="input-field col s6">

              <button type="submit" className="btn waves-effect waves-light" name="action">
                <i className="material-icons right"></i>Submit
              </button>

            </div>
          </div>
        </form>
      </div>
    )
  }
}