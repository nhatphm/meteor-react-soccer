import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import { List, ListItem } from 'material-ui/List';
import { Divider } from 'material-ui/Divider';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import Player from './Player.jsx';
import TeamList from './Team-list.jsx';
import TeamStats from './Team-stats.jsx';
import {Players} from '../api/players.js';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: []
    }
  }
  getPlayers() {
    return [
      {
        _id: 1,
        name: 'Nhat Pham',
        ballManipulation: '4',
        passAbility: '4',
        defense: '3'
      },
      {
        _id: 2,
        name: 'Nam Nguyen',
        ballManipulation: '4',
        passAbility: '4',
        defense: '3'
      },
      {
        _id: 3,
        name: 'Hoai Le',
        ballManipulation: '4',
        passAbility: '4',
        defense: '3'
      },
      {
        _id: 4,
        name: 'Thuan Nguyen',
        ballManipulation: '4',
        passAbility: '4',
        defense: '3'
      },
      {
        _id: 5,
        name: 'Giang Pham',
        ballManipulation: '4',
        passAbility: '4',
        defense: '3'
      },
    ];
  }
  renderPlayers() {
    return this.props.players.map((player) => {
      return <TeamList key={player._id} player={player} />
    });
  }
  render() {
    return (
      <MuiThemeProvider>
        <div className="container">
          <AppBar
            title="Soccer Application"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            showMenuIconButton={false}
          />
          <div className="row">
            <div className="col s12 m7"><Player /></div>
            <div className="col s12 m5"><TeamStats /></div>
            <div className="col s12 m5">
              <h3>TeamList</h3> <Link to="/new" className="btn waves-effect waves-light">Add Player</Link>
              <List>
                {this.renderPlayers()}
              </List>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}
App.propTypes = {
  players: PropTypes.array.isRequired
}
export default createContainer(()=>{
  Meteor.subscribe('players');
  return {
    players: Players.find().fetch()
  }
},App);
