import React, { Component } from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import ReactDOM from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import { blue200, blue900 } from 'material-ui/styles/colors';
const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    margin: 12
  }
};

function handleRequestDelete() {
  alert('You clicked the delete button.');
}

function handleTouchTap() {
  alert('You clicked the Chip.');
}
export default class Player extends Component {

  render() {

    return (
      <Card>
        <CardMedia
          overlay={<CardTitle title="Pham Hoang Minh Nhat" subtitle="Offense: 12 - Defense: 9" />}
        >
          <img src="minhnhat.jpg" alt="" />
        </CardMedia>
        <CardTitle title="Card title" subtitle="Card subtitle" />

        <CardText>
          <div style={styles.wrapper}>
            <Chip
              backgroundColor={blue200}
              onRequestDelete={handleRequestDelete}
              onTouchTap={handleTouchTap}
              style={styles.chip}
            >
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                1
          </Avatar>
              Passing Abilities
        </Chip>
            <Chip
              backgroundColor={blue200}
              onRequestDelete={handleRequestDelete}
              onTouchTap={handleTouchTap}
              style={styles.chip}
            >
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                2
          </Avatar>
              Ball Manipulation
        </Chip>
            <Chip
              backgroundColor={blue200}
              onRequestDelete={handleRequestDelete}
              onTouchTap={handleTouchTap}
              style={styles.chip}
            >
              <Avatar size={32} color={blue200} backgroundColor={blue900}>
                3
          </Avatar>
              Defense
        </Chip>
          </div>
        </CardText>
      </Card>
    )
  }
}