import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

export const Players = new Mongo.Collection('players');
if (Meteor.isServer) {
  Meteor.publish('players', function () {
    return Players.find();
  })
}
export default createContainer(() => {
  Meteor.subscribe('players');
  return {
    players: Players.find().fetch(),
  }
});
// const PlayerSchema = new SimpleSchema({
//   name: { type: String },
//   team: { type: String },
//   ballManipulation: { type: Number, defaultValue: 1 },
//   passingAbilities: { type: Number, defaultValue: 1 },
//   notes: { type: String, optional: true },
// });

// Players.attachSchema(PlayerSchema);