import React from 'react';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
import { Route, Router ,Switch} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';
import App from '../imports/ui/App.jsx';
import Example from '../imports/ui/Example.jsx';
import Lost from '../imports/ui/Lost.jsx';
import browserHistory from '../imports/api/history';

injectTapEventPlugin();
Meteor.startup(() => {
  ReactDOM.render(
    <Router history={browserHistory}>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/new' component={Example} />
        <Route path='*' component={Lost} />
      </Switch>
    </Router>
    , document.getElementById('render-target'));
})