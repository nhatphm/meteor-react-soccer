import { Meteor } from 'meteor/meteor';
import { Players } from '../imports/api/players.js';

Meteor.startup(() => {
  Meteor.methods({
    'players.insert'(data) {
      console.log(data);
      Players.insert({
        name: data.name,
        team: data.team,
        ballManipulation: data.ballManipulation,
        passingAbilities: data.passingAbilities,
        notes: data.notes,
        createAt: data.createAt,
      })
    }
  });
  Meteor.methods({
    'players.find'() {
      console.log('fined');
     return Players.find().fetch();
    }
  });
  // code to run on server at startup
});
